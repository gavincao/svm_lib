#ifndef _SMO_H_
#define _SMO_H_
#include<vector>
#include"SVM.h"

using namespace std;

/*
* get next optimization pair
*/
class OptPairGen{
public:
	/*
	* get next optimization
	*/
	static pair<int,int> getNext(int cur,int featureNum);
};

/*
* smo algorithm
*/
class SMO{
public:
	SMO(vector<SMPItem> &smps,KernelType kt);
	/*
	* train svm
	*/
	ModelParameter train(int maxIter,double C,double tolerate);
	/*
	* convert alfas to ws can be only used to KT_LINEAR
	*/
	void alfaToW(ModelParameter &mp);
protected:
	/*
	* get f(x_i)
	*/
	double getfXi(int i,ModelParameter &mp);
	/*
	* terminate condition test
	*/
	bool canStop(double C,ModelParameter oldMP,ModelParameter newMP,double tolerate);
	/*
	* get kernel value
	*/
	double getKij(int i,int j);
	/*
	* init model parameter
	*/
	void initMP(ModelParameter &mp);
	/*
	* get box parameter
	*/
	pair<double,double> getHL(int i,int j,ModelParameter mpOld,double C);
	/*
	* clip alfa
	*/
	double getAlfajClipped(pair<double,double> hl,double alfajNew);
protected:
	/*
	* samples vector
	*/
	vector<SMPItem> _smps;
	/*
	* kernel type
	*/
	KernelType _kt;
};
#endif//~
