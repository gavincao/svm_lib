# -*- coding: utf-8 -*-
"""
Created on Mon Jun 29 15:02:33 2015

@author: caoyuji
"""
import matplotlib.pyplot as plot
class Draw:
    def loadDataSet(self,fileName):
        dataMat = []; labelMat = []
        fr = open(fileName)
        for line in fr.readlines():
            lineArr = line.strip().split('\t')
            dataMat.append([float(lineArr[0]), float(lineArr[1])])
            labelMat.append(float(lineArr[2]))
        return dataMat,labelMat
    def draw(self,dataMat,labelMat):
        x=list()
        y=list()
        for i in range(len(dataMat)):
            if labelMat[i]==-1.0:
                plot.plot(dataMat[i][0],dataMat[i][1],'bo')
            else:
                plot.plot(dataMat[i][0],dataMat[i][1],'r+')
            #plot.plot(dataMat[i][0],(0.70*dataMat[i][0]-3.17692)/0.216,'y+')
            x.append(dataMat[i][0]);
            y.append((0.4149*dataMat[i][0]+0.41065)/(-2.315))
        plot.plot(x,y)
        plot.show()
        
        
d=Draw()
dM,lM=d.loadDataSet("../dat/testSetRBF.txt")
d.draw(dM,lM)                            
