#include"SVMUtil.h"
#include<iostream>
using namespace std;
int main(){
	string fileName("./dat/testSetPOLY.txt");
	SMPLoader smpLoader(fileName);
	vector<SMPItem> smpVec=smpLoader.load();
	/*
	pair<int,int> p(0,1);
	int i=0;
	while(i<2000){
		cout<<p.first<<"\t"<<p.second<<endl;
		p=OptPairGen::getNext(i,smpVec.size());
		i++;
	}
	*/
	SMO smo(smpVec,KT_POLY);
	ModelParameter mp=smo.train(50000,6,0.00001);
	for(int i=0;i<mp._alfas.size();++i){
		cout<<mp._alfas[i]<<"\n";
	}
	SVMUtil svmUtil(KT_POLY);
	int num=0;
	for(int i=0;i<smpVec.size();++i){
		if(svmUtil.testModel(mp,smpVec[i],smpVec)){
			num++;
		}
	}
	cout<<num<<"\t"<<smpVec.size()<<endl;
	/*
	smo.alfaToW(mp);
	for(int i=0;i<mp._w.size();++i){
		cout<<mp._w[i]<<endl;
	}
	cout<<mp._b<<endl;
	*/
	return 1;
}
