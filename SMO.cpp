#include<vector>
#include<iostream>
#include<cstdlib>
#include<cmath>
#include<ctime>
#include<algorithm>
#include"SMO.h"
using namespace std;

pair<int,int> OptPairGen::getNext(int i,int smpNum){
	i=i%smpNum;
	int j=i;
	while(j==i){
		j=((double)(rand()%smpNum));
	}
	pair<int,int> ret(i,j);
	return ret;
}
SMO::SMO(vector<SMPItem> &smps,KernelType kt){
	_smps=smps;
	_kt=kt;
}
ModelParameter SMO::train(int maxIter,double C,double tolerate){
	ModelParameter curMP;
	if(_smps.size()<=0){
		throw "no sample items for train";
	}
	
	//init model parameter
	initMP(curMP);
	
	//main iterator
	int smpNum=_smps.size();
	pair<int,int> curIndex(0,1);
	int iter=0;
	ModelParameter oldMP=curMP;
	while(iter<maxIter){
		//iterate
		curIndex=OptPairGen::getNext(iter,smpNum);
		int i=curIndex.first;
		double alfaOldi=oldMP._alfas[i];
		double Ei=getfXi(i,oldMP)-_smps[i]._lbl;
		//check if ai satisfiy KKT
		if((alfaOldi>0&&Ei*_smps[i]._lbl>tolerate)||		
			(alfaOldi<C&&Ei*_smps[i]._lbl<-tolerate))
		{//not		
			int j=curIndex.second;
			double alfaOldj=oldMP._alfas[j];
			double Ej=getfXi(j,oldMP)-_smps[j]._lbl;

			pair<double,double>hl= getHL(i,j,oldMP,C);
			//get new alfaJ
			double eta=getKij(i,i)+getKij(j,j)-2*getKij(i,j);
			if(eta<=0.0)continue;
			curMP._alfas[j]=oldMP._alfas[j]+(_smps[j]._lbl*(Ei-Ej))/eta;
			curMP._alfas[j]=getAlfajClipped(hl,curMP._alfas[j]);
			if(fabs(curMP._alfas[j]-oldMP._alfas[j])<0.00001){
				iter++;
				continue;
			}	
			//get new alfaI
			curMP._alfas[i]=oldMP._alfas[i]+(_smps[i]._lbl*_smps[j]._lbl*(oldMP._alfas[j]-curMP._alfas[j]));
	
			//get new b
			double bNew1=curMP._b-Ei-
				_smps[i]._lbl*(curMP._alfas[i]-alfaOldi)*getKij(i,i)-
				_smps[j]._lbl*(curMP._alfas[j]-alfaOldj)*getKij(i,j);
			
			double bNew2=curMP._b-Ej-
				_smps[i]._lbl*(curMP._alfas[i]-alfaOldi)*getKij(i,j)-
				_smps[j]._lbl*(curMP._alfas[j]-alfaOldj)*getKij(j,j);
	
			if(curMP._alfas[i]>0&&curMP._alfas[i]<C){
				curMP._b=bNew1;
			}if(curMP._alfas[j]>0&&curMP._alfas[j]<C){
				curMP._b=bNew2;
			}else{
				curMP._b=(bNew1+bNew2)/2.0;
			}
	
			//stop judge
			if(iter!=0&&canStop(C,oldMP,curMP,tolerate)){
				break;
			}
		}	
		//prepare for next iterate
		iter++;
		oldMP=curMP;
	}	
	return curMP;
}	
void SMO::alfaToW(ModelParameter &mp){
	if(_kt==KT_LINEAR){	
		vector<double> &w=mp._w;
		for(int i=0;i<_smps[0]._features.size();++i){
			double tmp=0.0;
			for(int j=0;j<_smps.size();++j){
				tmp+=mp._alfas[j]*_smps[j]._lbl*_smps[j]._features[i];
			}
			w.push_back(tmp);
		}
	}else{
		throw "can not convert from alfa to W for not KT_LINEAR type";
	}
	
}
bool SMO::canStop(double C,ModelParameter oldMP,ModelParameter newMP,double tolerate){
	double alfaSum=0.0;
	for(int i=0;i<newMP._alfas.size();++i){
		alfaSum+=newMP._alfas[i];
	}
	double kernelSum=0.0;
	for(int i=0;i<newMP._alfas.size();++i){
		for(int j=0;j<newMP._alfas.size();++j){
			kernelSum+=_smps[i]._lbl*_smps[j]._lbl*newMP._alfas[i]*newMP._alfas[j]*getKij(i,j);
		}
	}
	double newW=alfaSum-kernelSum*0.5;

	cout<<newW<<endl;	
	return false;
}
double SMO::getKij(int i,int j){
	SMPItem iS=_smps[i];
	SMPItem jS=_smps[j];
	double ret=0.0;
	if(_kt==KT_LINEAR){
		for(int ii=0;ii<iS._features.size();++ii){
			ret+=iS._features[ii]*jS._features[ii];
		}	
	}else if(_kt==KT_POLY){
		double retTmp=0.0;
		for(int ii=0;ii<iS._features.size();++ii){
			retTmp+=iS._features[ii]*jS._features[ii];
		}	
		ret=pow(retTmp+1,2);
	}
	
	return ret;
}
void SMO::initMP(ModelParameter &mp){
	for(int i=0;i<_smps.size();++i){	
		mp._alfas.push_back(0.0);
	}		
	mp._b=0.0;
}
double SMO::getfXi(int i,ModelParameter &mp){
	double fXi=0.0;
	for(int j=0;j<_smps.size();++j){	
		fXi+=_smps[j]._lbl*mp._alfas[j]*getKij(i,j);
	}
	return fXi+mp._b;
}
pair<double,double> SMO::getHL(int i,int j,ModelParameter mpOld,double C){
	double L=0.0;
	double H=0.0;
	int yi=_smps[i]._lbl;
	int yj=_smps[j]._lbl;
	if(yi*yj==-1){
		L=max((double)0,(double)(mpOld._alfas[j]-mpOld._alfas[i]));
		H=min((double)C,(double)(C+mpOld._alfas[j]-mpOld._alfas[i]));
	}else{
		L=max((double)0,(double)(mpOld._alfas[j]+mpOld._alfas[i]-C));
		H=min((double)C,(double)(mpOld._alfas[j]+mpOld._alfas[i]));
	}	
	pair<double,double> ret(H,L);
	return ret;
}
double SMO::getAlfajClipped(pair<double,double> hl,double alfajNew){
	if(alfajNew>=hl.first){
		return hl.first;
	}else if(alfajNew>hl.second){
		return alfajNew;
	}else{
		return hl.second;
	}
}
