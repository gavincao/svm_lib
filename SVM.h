#ifndef _SMP_ITEM_H_
#define _SMP_ITEM_H_
#include<vector>
using namespace std;

/*
* sample item
*/
struct SMPItem{
	SMPItem():_lbl(0){}
	vector<double> _features;
	int _lbl;
	SMPItem(const SMPItem &right){
		this->_features.clear();
		for(vector<double>::const_iterator it=right._features.begin();it!=right._features.end();++it){
			this->_features.push_back(*it);
		}
		this->_lbl=right._lbl;
	
	}
	SMPItem& operator=(const SMPItem &right){
		this->_features.clear();
		for(vector<double>::const_iterator it=right._features.begin();it!=right._features.end();++it){
			this->_features.push_back(*it);
		}
		this->_lbl=right._lbl;
		return *this;
	}
};

/*
* svm model parameters
*/
struct ModelParameter{
	ModelParameter():_b(0.0){}
	double _b;
	vector<double> _w;
	vector<double> _alfas;
	ModelParameter &operator=(const ModelParameter &right){
		this->_alfas.clear();
		for(vector<double>::const_iterator it=right._alfas.begin();it!=right._alfas.end();++it){
			this->_alfas.push_back(*it);
		}

		this->_w.clear();
		for(vector<double>::const_iterator it=right._w.begin();it!=right._w.end();++it){
			this->_w.push_back(*it);
		}

		this->_b=right._b;
		return *this;
	}
	ModelParameter(const ModelParameter &right){
		this->_alfas.clear();
		for(vector<double>::const_iterator it=right._alfas.begin();it!=right._alfas.end();++it){
			this->_alfas.push_back(*it);
		}

		this->_w.clear();
		for(vector<double>::const_iterator it=right._w.begin();it!=right._w.end();++it){
			this->_w.push_back(*it);
		}

		this->_b=right._b;
	}
};

/*
* kernel type
*/
enum KernelType{
	KT_LINEAR=0,
	KT_POLY=1
};
#endif//~
