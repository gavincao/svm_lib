#ifndef _SVM_UTIL_H_
#define _SVM_UTIL_H_
#include"SMO.h"
#include"SVM.h"
#include"SMPLoader.h"
#include<vector>
#include<cmath>
using namespace std;
class SVMUtil{
public:
	SVMUtil(KernelType kt){
		_kt=kt;
	}

	double getKij(const vector<double> &vi,const vector<double> &vj);
	double getfXi(const ModelParameter &mp,const vector<double> &feature,const vector<SMPItem> &smps);
	bool testModel(const ModelParameter &mp,const SMPItem &smpItem,const vector<SMPItem> &smps);

	KernelType _kt;
};
#endif//~
