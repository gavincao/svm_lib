#include"SVMUtil.h"
#include<vector>
#include<iostream>
double SVMUtil::getKij(const vector<double> &vi,const vector<double> &vj){
	double ret=0.0;
	if(_kt==KT_LINEAR){
		for(int ii=0;ii<vi.size();++ii){
			ret+=vi[ii]*vj[ii];
		}	
	}else if(_kt==KT_POLY){
		double retTmp=0.0;
		for(int ii=0;ii<vi.size();++ii){
			retTmp+=vi[ii]*vj[ii];
		}	
		ret=pow(retTmp+1,2);
	}
	return ret;

}
double SVMUtil::getfXi(const ModelParameter &mp,const vector<double> &feature,const vector<SMPItem> &smps){
	double fXi=0.0;
	for(int j=0;j<smps.size();++j){	
		fXi+=smps[j]._lbl*mp._alfas[j]*getKij(feature,smps[j]._features);
	}
	return fXi+mp._b;

}
bool SVMUtil::testModel(const ModelParameter &mp,const SMPItem &smpItem,const vector<SMPItem> &smps){
	double ret=getfXi(mp,smpItem._features,smps);
	cout<<ret<<"\t"<<smpItem._lbl<<endl;
	if((ret*smpItem._lbl)>0.0){
		return true;
	}
	return false;
}
