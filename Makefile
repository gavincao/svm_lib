CPP:=g++
CFLAGS:=-c
LDFLAGS:=-g
RM:=rm -rf
PRJNAME:=svm

all:${PRJNAME}

.cpp.o:
	${CPP} ${CFLAGS} $< -g

${PRJNAME}:SMO.o SMPLoader.o SVMUtil.o Main.o
	${CPP} ${LDFLAGS} -o $@ $^


.PHONY:clean

clean:
	${RM} *.o
	${RM} ${PRJNAME}
