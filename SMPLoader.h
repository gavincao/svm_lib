#ifndef _SMP_LOADER_H_
#define _SMP_LOADER_H_
#include<string>
#include"SVM.h"
using namespace std;
/*
* sample loader
*/
class SMPLoader{
public:
	SMPLoader(const string smpName);
	vector<SMPItem> load();
protected:
	vector<string> split(const string& str, string sep = ",");
	string _smpName;
};
#endif//~
